package com.aims.mybatisplus.dao;

import com.aims.mybatisplus.model.entity.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Mapper
public interface MemberMapper extends BaseMapper<Member> {


}