package com.lglbc.oauth2;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author： 乐哥聊编程(全平台同号)
 * @date: 2022/5/27
 */
@RestController
public class Index {
    @RequestMapping
    public String hello(){
        return "hello";
    }
    @RequestMapping("/user/hello")
    public String helloUser(){
        return "hello user";
    }
    @RequestMapping("/admin/hello")
    public String helloAdmin(){
        return "hello admin";
    }
}
