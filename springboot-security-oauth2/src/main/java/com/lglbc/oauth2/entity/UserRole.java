package com.lglbc.oauth2.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 用户和角色关联表
 * @author zhengkai.blog.csdn.net
 * @date 2022-05-27
 */
@Data
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 用户id
    */
    private Integer userId;

    /**
    * 角色id
    */
    private Integer roleId;

    public UserRole() {}
}