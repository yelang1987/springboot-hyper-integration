package com.lglbc.oauth2.common.result;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
public interface IResultCode {

    String getCode();

    String getMsg();

}
