package com.lglbc.oauth2.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lglbc.oauth2.entity.OauthClient;
import com.lglbc.oauth2.entity.Role;
import com.lglbc.oauth2.mapper.OauthClientMapper;
import com.lglbc.oauth2.mapper.RoleMapper;
import com.lglbc.oauth2.service.OauthClientService;
import com.lglbc.oauth2.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Service
@RequiredArgsConstructor
public class OauthClientServiceImpl extends ServiceImpl<OauthClientMapper, OauthClient> implements OauthClientService {
}
