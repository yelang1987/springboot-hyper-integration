package com.lglbc.oauth2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lglbc.oauth2.entity.OauthClient;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author zhengkai.blog.csdn.net
 * @description oauth_clientMapper
 * @date 2022-05-28
 */
@Mapper
public interface OauthClientMapper extends BaseMapper<OauthClient> {

    @Select(
            "<script>select t0.* from oauth_client t0 " +
                    //add here if need left join
                    "where 1=1" +
                    "<when test='clientId!=null and clientId!=&apos;&apos; '> and t0.client_id=#{clientId}</when> " +
                    "<when test='resourceIds!=null and resourceIds!=&apos;&apos; '> and t0.resource_ids=#{resourceIds}</when> " +
                    "<when test='clientSecret!=null and clientSecret!=&apos;&apos; '> and t0.client_secret=#{clientSecret}</when> " +
                    "<when test='scope!=null and scope!=&apos;&apos; '> and t0.scope=#{scope}</when> " +
                    "<when test='authorizedGrantTypes!=null and authorizedGrantTypes!=&apos;&apos; '> and t0.authorized_grant_types=#{authorizedGrantTypes}</when> " +
                    "<when test='webServerRedirectUri!=null and webServerRedirectUri!=&apos;&apos; '> and t0.web_server_redirect_uri=#{webServerRedirectUri}</when> " +
                    "<when test='authorities!=null and authorities!=&apos;&apos; '> and t0.authorities=#{authorities}</when> " +
                    "<when test='accessTokenValidity!=null and accessTokenValidity!=&apos;&apos; '> and t0.access_token_validity=#{accessTokenValidity}</when> " +
                    "<when test='refreshTokenValidity!=null and refreshTokenValidity!=&apos;&apos; '> and t0.refresh_token_validity=#{refreshTokenValidity}</when> " +
                    "<when test='additionalInformation!=null and additionalInformation!=&apos;&apos; '> and t0.additional_information=#{additionalInformation}</when> " +
                    "<when test='autoapprove!=null and autoapprove!=&apos;&apos; '> and t0.autoapprove=#{autoapprove}</when> " +
                    "<when test='createTime!=null and createTime!=&apos;&apos; '> and t0.create_time=#{createTime}</when> " +
                    "<when test='updateTime!=null and updateTime!=&apos;&apos; '> and t0.update_time=#{updateTime}</when> " +
                    "<when test='updateBy!=null and updateBy!=&apos;&apos; '> and t0.update_by=#{updateBy}</when> " +
                    "<when test='createBy!=null and createBy!=&apos;&apos; '> and t0.create_by=#{createBy}</when> " +
                    //add here if need page limit
                    //" limit ${page},${limit} " +
                    " </script>")
    List<OauthClient> pageAll(OauthClient queryParamDTO, int page, int limit);

    @Select("<script>select count(1) from oauth_client t0 " +
            //add here if need left join
            "where 1=1" +
            "<when test='clientId!=null and clientId!=&apos;&apos; '> and t0.client_id=#{clientId}</when> " +
            "<when test='resourceIds!=null and resourceIds!=&apos;&apos; '> and t0.resource_ids=#{resourceIds}</when> " +
            "<when test='clientSecret!=null and clientSecret!=&apos;&apos; '> and t0.client_secret=#{clientSecret}</when> " +
            "<when test='scope!=null and scope!=&apos;&apos; '> and t0.scope=#{scope}</when> " +
            "<when test='authorizedGrantTypes!=null and authorizedGrantTypes!=&apos;&apos; '> and t0.authorized_grant_types=#{authorizedGrantTypes}</when> " +
            "<when test='webServerRedirectUri!=null and webServerRedirectUri!=&apos;&apos; '> and t0.web_server_redirect_uri=#{webServerRedirectUri}</when> " +
            "<when test='authorities!=null and authorities!=&apos;&apos; '> and t0.authorities=#{authorities}</when> " +
            "<when test='accessTokenValidity!=null and accessTokenValidity!=&apos;&apos; '> and t0.access_token_validity=#{accessTokenValidity}</when> " +
            "<when test='refreshTokenValidity!=null and refreshTokenValidity!=&apos;&apos; '> and t0.refresh_token_validity=#{refreshTokenValidity}</when> " +
            "<when test='additionalInformation!=null and additionalInformation!=&apos;&apos; '> and t0.additional_information=#{additionalInformation}</when> " +
            "<when test='autoapprove!=null and autoapprove!=&apos;&apos; '> and t0.autoapprove=#{autoapprove}</when> " +
            "<when test='createTime!=null and createTime!=&apos;&apos; '> and t0.create_time=#{createTime}</when> " +
            "<when test='updateTime!=null and updateTime!=&apos;&apos; '> and t0.update_time=#{updateTime}</when> " +
            "<when test='updateBy!=null and updateBy!=&apos;&apos; '> and t0.update_by=#{updateBy}</when> " +
            "<when test='createBy!=null and createBy!=&apos;&apos; '> and t0.create_by=#{createBy}</when> " +
            " </script>")
    int countAll(OauthClient queryParamDTO);

}