package com.lglbc.oauth2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lglbc.oauth2.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 用户信息表Mapper
 * @author zhengkai.blog.csdn.net
 * @date 2022-05-27
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select(
    "<script>select t0.* from user t0 " +
    //add here if need left join
    "where 1=1" +
    "<when test='id!=null and id!=&apos;&apos; '> and t0.id=#{id}</when> " +
    "<when test='username!=null and username!=&apos;&apos; '> and t0.username=#{username}</when> " +
    "<when test='nickname!=null and nickname!=&apos;&apos; '> and t0.nickname=#{nickname}</when> " +
    "<when test='gender!=null and gender!=&apos;&apos; '> and t0.gender=#{gender}</when> " +
    "<when test='password!=null and password!=&apos;&apos; '> and t0.password=#{password}</when> " +
    "<when test='avatar!=null and avatar!=&apos;&apos; '> and t0.avatar=#{avatar}</when> " +
    "<when test='mobile!=null and mobile!=&apos;&apos; '> and t0.mobile=#{mobile}</when> " +
    "<when test='status!=null and status!=&apos;&apos; '> and t0.status=#{status}</when> " +
    "<when test='email!=null and email!=&apos;&apos; '> and t0.email=#{email}</when> " +
    "<when test='deleted!=null and deleted!=&apos;&apos; '> and t0.deleted=#{deleted}</when> " +
    "<when test='createTime!=null and createTime!=&apos;&apos; '> and t0.create_time=#{createTime}</when> " +
    "<when test='updateTime!=null and updateTime!=&apos;&apos; '> and t0.update_time=#{updateTime}</when> " +
    "<when test='updateBy!=null and updateBy!=&apos;&apos; '> and t0.update_by=#{updateBy}</when> " +
    "<when test='createBy!=null and createBy!=&apos;&apos; '> and t0.create_by=#{createBy}</when> " +
    //add here if need page limit
    //" limit ${page},${limit} " +
    " </script>")
    List<User> pageAll(User queryParamDTO, int page, int limit);

    @Select("<script>select count(1) from user t0 " +
    //add here if need left join
    "where 1=1" +
    "<when test='id!=null and id!=&apos;&apos; '> and t0.id=#{id}</when> " +
    "<when test='username!=null and username!=&apos;&apos; '> and t0.username=#{username}</when> " +
    "<when test='nickname!=null and nickname!=&apos;&apos; '> and t0.nickname=#{nickname}</when> " +
    "<when test='gender!=null and gender!=&apos;&apos; '> and t0.gender=#{gender}</when> " +
    "<when test='password!=null and password!=&apos;&apos; '> and t0.password=#{password}</when> " +
    "<when test='avatar!=null and avatar!=&apos;&apos; '> and t0.avatar=#{avatar}</when> " +
    "<when test='mobile!=null and mobile!=&apos;&apos; '> and t0.mobile=#{mobile}</when> " +
    "<when test='status!=null and status!=&apos;&apos; '> and t0.status=#{status}</when> " +
    "<when test='email!=null and email!=&apos;&apos; '> and t0.email=#{email}</when> " +
    "<when test='deleted!=null and deleted!=&apos;&apos; '> and t0.deleted=#{deleted}</when> " +
    "<when test='createTime!=null and createTime!=&apos;&apos; '> and t0.create_time=#{createTime}</when> " +
    "<when test='updateTime!=null and updateTime!=&apos;&apos; '> and t0.update_time=#{updateTime}</when> " +
    "<when test='updateBy!=null and updateBy!=&apos;&apos; '> and t0.update_by=#{updateBy}</when> " +
    "<when test='createBy!=null and createBy!=&apos;&apos; '> and t0.create_by=#{createBy}</when> " +
     " </script>")
    int countAll(User queryParamDTO);

}