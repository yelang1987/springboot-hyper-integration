package com.lglbc.oauth2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lglbc.oauth2.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 角色表Mapper
 * @author zhengkai.blog.csdn.net
 * @date 2022-05-27
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    @Select(
    "<script>select t0.* from role t0 " +
    //add here if need left join
    "where 1=1" +
    "<when test='id!=null and id!=&apos;&apos; '> and t0.id=#{id}</when> " +
    "<when test='name!=null and name!=&apos;&apos; '> and t0.name=#{name}</when> " +
    "<when test='code!=null and code!=&apos;&apos; '> and t0.code=#{code}</when> " +
    "<when test='sort!=null and sort!=&apos;&apos; '> and t0.sort=#{sort}</when> " +
    "<when test='status!=null and status!=&apos;&apos; '> and t0.status=#{status}</when> " +
    "<when test='deleted!=null and deleted!=&apos;&apos; '> and t0.deleted=#{deleted}</when> " +
    "<when test='createTime!=null and createTime!=&apos;&apos; '> and t0.create_time=#{createTime}</when> " +
    "<when test='updateTime!=null and updateTime!=&apos;&apos; '> and t0.update_time=#{updateTime}</when> " +
    "<when test='updateBy!=null and updateBy!=&apos;&apos; '> and t0.update_by=#{updateBy}</when> " +
    "<when test='createBy!=null and createBy!=&apos;&apos; '> and t0.create_by=#{createBy}</when> " +
    //add here if need page limit
    //" limit ${page},${limit} " +
    " </script>")
    List<Role> pageAll(Role queryParamDTO, int page, int limit);

    @Select("<script>select count(1) from role t0 " +
    //add here if need left join
    "where 1=1" +
    "<when test='id!=null and id!=&apos;&apos; '> and t0.id=#{id}</when> " +
    "<when test='name!=null and name!=&apos;&apos; '> and t0.name=#{name}</when> " +
    "<when test='code!=null and code!=&apos;&apos; '> and t0.code=#{code}</when> " +
    "<when test='sort!=null and sort!=&apos;&apos; '> and t0.sort=#{sort}</when> " +
    "<when test='status!=null and status!=&apos;&apos; '> and t0.status=#{status}</when> " +
    "<when test='deleted!=null and deleted!=&apos;&apos; '> and t0.deleted=#{deleted}</when> " +
    "<when test='createTime!=null and createTime!=&apos;&apos; '> and t0.create_time=#{createTime}</when> " +
    "<when test='updateTime!=null and updateTime!=&apos;&apos; '> and t0.update_time=#{updateTime}</when> " +
    "<when test='updateBy!=null and updateBy!=&apos;&apos; '> and t0.update_by=#{updateBy}</when> " +
    "<when test='createBy!=null and createBy!=&apos;&apos; '> and t0.create_by=#{createBy}</when> " +
     " </script>")
    int countAll(Role queryParamDTO);

}