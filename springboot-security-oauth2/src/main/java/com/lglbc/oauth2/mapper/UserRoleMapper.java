package com.lglbc.oauth2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lglbc.oauth2.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 用户和角色关联表Mapper
 * @author zhengkai.blog.csdn.net
 * @date 2022-05-27
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

    @Select(
    "<script>select t0.* from user_role t0 " +
    //add here if need left join
    "where 1=1" +
    "<when test='userId!=null and userId!=&apos;&apos; '> and t0.user_id=#{userId}</when> " +
    "<when test='roleId!=null and roleId!=&apos;&apos; '> and t0.role_id=#{roleId}</when> " +
    //add here if need page limit
    //" limit ${page},${limit} " +
    " </script>")
    List<UserRole> pageAll(UserRole queryParamDTO, int page, int limit);

    @Select("<script>select count(1) from user_role t0 " +
    //add here if need left join
    "where 1=1" +
    "<when test='userId!=null and userId!=&apos;&apos; '> and t0.user_id=#{userId}</when> " +
    "<when test='roleId!=null and roleId!=&apos;&apos; '> and t0.role_id=#{roleId}</when> " +
     " </script>")
    int countAll(UserRole queryParamDTO);
    @Select("select role_id from user_role where user_id = #{userId}")
    List<String>  getRoleIdByUserId(@Param("userId") Long userId);
}