package com.ams.springbootnacos;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@RestController
@RequestMapping("/test")
public class IndexController {
    @NacosValue(value = "${ams.testContent}", autoRefreshed = true)
    private String testContent;

    @RequestMapping("/config")
    public String config(){
        return "配置中心的内容"+testContent;
    }
}
