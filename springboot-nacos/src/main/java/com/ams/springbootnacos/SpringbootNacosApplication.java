package com.ams.springbootnacos;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@SpringBootApplication
@NacosPropertySource(dataId = "springBoot-nacos", autoRefreshed = true)
public class SpringbootNacosApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootNacosApplication.class, args);
    }

}
