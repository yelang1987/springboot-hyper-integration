package com.lglbc.oauth2;

import com.lglbc.oauth2.common.UserContextUtil;
import com.nimbusds.jose.JWSObject;
import com.xiaoleilu.hutool.json.JSONUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;

/**
 * @author： 乐哥聊编程(全平台同号)
 * @date: 2022/5/28
 */
@RestController
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/read")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String read(){
        System.out.println("read");
        return "read";
    }
    @RequestMapping("/write")
//    @Secured({"ROLE_ADMIN"})
    @PreAuthorize("hasAuthority('ROLE_ADMIN_WRITE')")
    public String write() throws ParseException {
        return UserContextUtil.getUserId()+"::"+UserContextUtil.getUserName();
    }
}
