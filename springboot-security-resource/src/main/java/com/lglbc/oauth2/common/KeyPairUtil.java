package com.lglbc.oauth2.common;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 * @author： 乐哥聊编程(全平台同号)
 * @date: 2022/5/29
 */
public class KeyPairUtil {
    public static String getPubKey() {
        Resource resource = new ClassPathResource("pub.txt");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
            System.out.println("本地公钥");
            return br.lines().collect(Collectors.joining("\n"));
        } catch (IOException ioe) {
            return "";
        }
    }
}
