package com.ams.sharding.jdbc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ams.sharding.jdbc.domain.Order;
import com.ams.sharding.jdbc.service.OrderService;
import com.ams.sharding.jdbc.mapper.OrderMapper;
import org.springframework.stereotype.Service;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order>
    implements OrderService{

}




